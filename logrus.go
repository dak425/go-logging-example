package main

import (
	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})
	log.WithFields(
		log.Fields{
			"Day":  "foo",
			"Time": "bar",
		},
	).Info("This is a json message")
	log.Info("This is some info")
	log.Info("This is some more info")
	log.Error("THIS IS AN ERROR")
}
